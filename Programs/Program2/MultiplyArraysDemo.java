import java.util.Scanner;
import java.util.Arrays;

public class MultiplyArraysDemo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of elements in the first array: ");
        int n = input.nextInt();
        int[] array1 = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i+1) + " of the first array: ");
            array1[i] = input.nextInt();
        }

        System.out.print("Enter the number of elements in the second array: ");
        int m = input.nextInt();
        int[] array2 = new int[m];
        for (int i = 0; i < m; i++) {
            System.out.print("Enter element " + (i+1) + " of the second array: ");
            array2[i] = input.nextInt();
        }
        
        if (n != m) {
            System.out.println("Error: Arrays have different length, not possible to multiply them.");
            return;
        }
        
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            result[i] = array1[i] * array2[i];
        }

        System.out.println("Result of multiplying arrays: " + Arrays.toString(result));
    }
}
