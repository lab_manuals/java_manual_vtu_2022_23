import java.util.Scanner;

public class QuadraticEquationDemo {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a: ");
        double a = input.nextDouble();

        System.out.print("Enter b: ");
        double b = input.nextDouble();

        System.out.print("Enter c: ");
        double c = input.nextDouble();

        double discriminant = b * b - 4 * a * c;
        if (discriminant > 0) {
            System.out.println("The equation has real and distinct roots.");
            double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);
            System.out.println("The roots are " + root1 + " and " + root2);
        } else if (discriminant == 0) {
            System.out.println("The equation has real and equal roots.");
            double root = -b / (2 * a);
            System.out.println("The root is " + root);
        } else {
            System.out.println("The equation has no real roots.");
            double realp = -b / (2 * a);
            double imagp = (Math.sqrt(-discriminant)) / (2 * a);
            System.out.printf("The roots are (%.2f + i%.4f) and (%.2f - i%.4f)\n", realp, imagp, realp, imagp);
        }
    }
}
