class PointType {
    private double x;
    private double y;

    // Constructor overloading
    public PointType() {
        this.x = 0;
        this.y = 0;
    }

    public PointType(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Method overloading to calculate distance between two PointType objects
    public double distance(PointType point) {
    	System.out.println("\nCalculating distance between two PointType objects");
        return Math.sqrt(Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2));
    }

    // Method overloading to calculate distance between a PointType object and a point specified by its coordinates
    public double distance(double x, double y) {
    	System.out.println("Calculating distance between a PointType object and a point specified by its coordinates");    
        return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
    }
    
    public void show(){
    	System.out.printf("(%.1g,%.1g)\n",x,y);
    }
}

class PointDemo{
    public static void main(String[] args) {
        PointType point1 = new PointType(1, 1);
        PointType point2 = new PointType(7, 9);
        System.out.print("\npoint1 coordinates :"); point1.show();
        System.out.print("point2 coordinates :"); point2.show();
        double dVal = point1.distance(point2);
        System.out.println("Distance between point1 and point2: " + dVal );

        PointType point3 = new PointType();
        System.out.print("\npoint3 coordinates :"); point3.show();  
        dVal = point3.distance(3,4);      
        System.out.println("Distance between point3 and (3, 4): " + dVal);
    }
}
