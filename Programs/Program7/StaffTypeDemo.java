class Staff {
    private int staffId;
    private String name;
    private String phone;
    private double salary;

    public Staff(int staffId, String name, String phone, double salary) {
        this.staffId = staffId;
        this.name = name;
        this.phone = phone;
        this.salary = salary;
    }

    public int getStaffId() {
        return staffId;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public double getSalary() {
        return salary;
    }

    public void DisplayInfo(){
        System.out.println("Name: " + this.getName());
        System.out.println("Staff ID: " + this.getStaffId());
        System.out.println("Phone: " + this.getPhone());
        System.out.println("Salary: " + this.getSalary());
    }
}

class Teaching extends Staff {
    private String domain;
    private int publications;

    public Teaching(int staffId, String name, String phone, double salary, String domain, int publications) {
        super(staffId, name, phone, salary);
        this.domain = domain;
        this.publications = publications;
    }

    public String getDomain() {
        return domain;
    }

    public int getPublications() {
        return publications;
    }
    
    public void DisplayInfo(){
        super.DisplayInfo();
        System.out.println("Domain: " + this.getDomain());
        System.out.println("Publications: " + this.getPublications());    
    }
}

class Technical extends Staff {
    private String skills;

    public Technical(int staffId, String name, String phone, double salary, String skills) {
        super(staffId, name, phone, salary);
        this.skills = skills;
    }

    public String getSkills() {
        return skills;
    }
    public void DisplayInfo(){
        super.DisplayInfo();
        System.out.println("Skills: " + this.getSkills());
     }    
}

class Contract extends Staff {
    private int period;

    public Contract(int staffId, String name, String phone, double salary, int period) {
        super(staffId, name, phone, salary);
        this.period = period;
    }

    public int getPeriod() {
        return period;
    }
    public void DisplayInfo(){
        super.DisplayInfo();
        System.out.println("Period: " + this.getPeriod()+" months");
    }
}



public class StaffTypeDemo {
    public static void main(String[] args) {
        Teaching t1 = new Teaching(1, "Rajesh Nayak", "9822546534", 75000, "Computer Science", 15);
        Teaching t2 = new Teaching(2, "Sita Devi", "8787432499", 80000, "Mathematics", 20);
        Teaching t3 = new Teaching(3, "John Peter", "8528734373", 85000, "Physics", 25);
        Technical te1 = new Technical(4,"Ramesha","9473673642",90000,"Java,Python,C++");
        Technical te2 = new Technical(5,"Suresha","8917612332",95000,"JavaScript,React,Node.js");
        Technical te3 = new Technical(6,"Dinesha","9944222323",100000,"Python,TensorFlow,Keras");
        Contract c1 = new Contract(7, "Abida Begum", "9323786211", 75000, 6);
        Contract c2 = new Contract(8, "Lily Thomas", "8776551219", 80000, 12);
        Contract c3 = new Contract(9, "Seema Jain", "9922324343", 85000, 18);
        //display the staff objects
        System.out.println("Teaching Staff 1:");
		t1.DisplayInfo();
        System.out.println("\nTeaching Staff 2:");
		t2.DisplayInfo();
        System.out.println("\nTeaching Staff 3:");
		t3.DisplayInfo();
       
        System.out.println("\nTechnical Staff 1:");
		te1.DisplayInfo();

        System.out.println("\nTechnical Staff 2:");
        te2.DisplayInfo();

        System.out.println("\nTechnical Staff 3:");
        te3.DisplayInfo();

        System.out.println("\nContract Staff 1:");
        c1.DisplayInfo();

        System.out.println("\nContract Staff 2:");
        c2.DisplayInfo();
       
        System.out.println("\nContract Staff 3:");
        c3.DisplayInfo();
    }
}

