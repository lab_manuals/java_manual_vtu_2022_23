
import com.P1.A;
import com.P1.B;
import com.P1.C;
import com.P2.D;
import com.P2.E;

public class PackageDemo {

    public static void main(String[] args) {

        // Creating an object of class A in package P1
        A a = new A();
        a.setX(10);
        a.setY(20);
        System.out.println("Value of x: " + a.getX());
        System.out.println("Value of y: " + a.getY());

        // Creating an object of class B in package P1
        B b = new B();
        b.setX(30);
        b.setY(40);
        b.setZ(50);
        System.out.println("Value of x: " + b.getX());
        System.out.println("Value of y: " + b.getY());
        System.out.println("Value of z: " + b.getZ());

        // Creating an object of class C in package P1
        C c = new C();
        c.setW(60);
        System.out.println("Value of w: " + c.getW());

        // Creating an object of class D in package P2
        D d = new D();
        d.setX(70);
        d.setY(80);
        d.setV(90);
        System.out.println("Value of x: " + d.getX());
        System.out.println("Value of y: " + d.getY());
        System.out.println("Value of v: " + d.getV());

        // Creating an object of class E in package P2
        E e = new E();
        e.setU(100);
        System.out.println("Value of u: " + e.getU());
    }
}

