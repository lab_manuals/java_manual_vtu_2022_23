// Package P2
package com.P2;

// Class D inherited from A with protected variable and public method
public class D extends com.P1.A {
    protected int v;

    public void setV(int v) {
        this.v = v;
    }

    public int getV() {
        return v;
    }
}


