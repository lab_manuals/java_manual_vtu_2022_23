// Package P2
package com.P2;

// Class E with private variable and public method
public class E {
    private int u;

    public void setU(int u) {
        this.u = u;
    }

    public int getU() {
        return u;
    }
}
