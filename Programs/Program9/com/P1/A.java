// Package P1
package com.P1;

// Class A with private and protected variables and public methods
public class A {
    private int x;
    protected int y;

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }
}



