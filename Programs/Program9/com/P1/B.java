// Package P1
package com.P1;


// Class B inherited from A with default variable and public method
public class B extends A {
    int z;

    public void setZ(int z) {
        this.z = z;
    }

    public int getZ() {
        return z;
    }
}

