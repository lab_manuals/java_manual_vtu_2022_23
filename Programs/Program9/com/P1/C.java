// Package P1
package com.P1;

// Class C with private variable and public method
public class C {
    private int w;

    public void setW(int w) {
        this.w = w;
    }

    public int getW() {
        return w;
    }
}

