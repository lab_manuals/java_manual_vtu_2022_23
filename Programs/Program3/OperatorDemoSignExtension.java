import java.util.Scanner;

class OperatorDemoSignExtension {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
    
        System.out.print("\nLeft Shift Operator\nEnter a number : ");
        int num = sc.nextInt();
        System.out.print("Enter the number of positions for the Left Shift Operation : ");
        int pos = sc.nextInt();

        int res = num << pos;
        System.out.println("\nResult of Left Shift Operation");
        System.out.printf("\n%d << %d = %d\n", num, pos, res);
         
        System.out.print("\nRight Shift Operator\nEnter a number : ");
        num = sc.nextInt();
        System.out.print("Enter the number of positions for the Right Shift Operation : ");
        pos = sc.nextInt();

        res = num >> pos;
        System.out.println("\nResult of Right Shift Operation");
        System.out.printf("\n%d >> %d = %d\n", num, pos, res); 
        if(num < 0){
            System.out.println("We observe that Even after the Right Shift operation the sign bit of negative number is preserved. We call this sign extension.");
        }       
        System.out.print("\nUnsigned Right Shift Operator\nEnter a number : ");
        num = sc.nextInt();
        System.out.print("Enter the number of positions for the Unsigned Right Shift Operation : ");
        pos = sc.nextInt();

        res = num >>> pos;
        System.out.println("\nResult of Unsigned Right Shift Operation");
        System.out.printf("\n%d >>> %d = %d\n", num, pos, res);                
    }
}
