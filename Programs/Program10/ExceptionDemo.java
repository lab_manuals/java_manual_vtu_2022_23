import java.util.Scanner;

public class ExceptionDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter two integers: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        try {
            if (b == 0) {
                throw new ArithmeticException("Division by zero");
            }
            int result = a / b;
            System.out.println("Result of integer division: " + result);
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
        try {
            int[] arr = new int[5];
            arr[10] = 50;	//invalid index generates exception  
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
    }
}

